<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use DB;

class AssetListExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $arrAssetId;

    function __construct($arrAssetId) {
        $this->arrAssetId = $arrAssetId;
    }

    public function collection()
    {
        $datas = DB::table('v_asset_list')
            ->whereIn('asset_code', $this->arrAssetId)
            ->select(
                "asset_code", 
                "asset_name",
                "type_descr",
                "brand",
                "spec",
                "sn",
                "nopol",
                "color",
                "cc",
                "category",
                "dept_name",
                "assigned_user",
                "purchase_cost",
                "currency",
                "purchase_date",
                "po_number",
                "supplier_name",
                "residual_value",
                "depreciation_method_descr",
                "usage_status",
                "condition_descr",
                "location_name",
                "building_name",
                "area_name",
                "floor",
                "department_name",
                "remarks"
            )
            ->get();
        
        return $datas;
    }

    public function headings(): array
    {
        return [
            "asset_code", 
            "asset_name",
            "type_descr",
            "brand",
            "spec",
            "sn",
            "nopol",
            "color",
            "cc",
            "category",
            "dept_name",
            "assigned_user",
            "purchase_cost",
            "currency",
            "purchase_date",
            "po_number",
            "supplier_name",
            "residual_value",
            "depreciation_method_descr",
            "usage_status",
            "condition_descr",
            "location_name",
            "building_name",
            "area_name",
            "floor",
            "department_name",
            "remarks"
        ];
    }
}
