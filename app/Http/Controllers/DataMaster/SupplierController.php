<?php

namespace App\Http\Controllers\DataMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class SupplierController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $data = DB::table('supplier')->where('deleted', 0)->get();
            $param = [
                'title' => 'Data Master > Supplier',
                'data' => $data
            ];
            return view('data-master.supplier.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Data Master > Supplier > Add'
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $messages = [
                    'name.unique' => 'Name field has already been taken',
                    'name.required' => 'Name field is required',
                ];
                $rules = [
                    'name' => 'required|string|min:3|max:100|unique:supplier,name'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('supplier')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('data-master.supplier.index')->withErrors($success);
            }


            return view('data-master.supplier.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function edit(Request $req) {
        try {
            $data = DB::table('supplier')->where('supplier_id', $req->id)->first();
            $param = [
                'title' => 'Data Master > Supplier > Edit',
                'data' => $data
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $messages = [
                    'name.unique' => 'Name field has already been taken',
                    'name.required' => 'Name field is required',
                ];
                $rules = [
                    'name' => 'required|string|min:3|max:100|unique:supplier,name,' . $req->id . ',supplier_id'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('supplier')->where('supplier_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('data-master.supplier.index')->withErrors($success);
            }

            return view('data-master.supplier.edit', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function delete(Request $req) {
        try {
            DB::table('supplier')->where('supplier_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('data-master.supplier.index')->withErrors($success);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
