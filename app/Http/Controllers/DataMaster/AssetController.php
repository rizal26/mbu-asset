<?php

namespace App\Http\Controllers\DataMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AssetController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $data = DB::table('m_asset as a')
                ->join('category as b', 'b.category_id', '=', 'a.category_id')
                ->select('a.*', 'b.name AS category_name')
                ->where('a.deleted', 0)->get();

            $param = [
                'title' => 'Data Master > Asset',
                'data' => $data
            ];
            return view('data-master.asset.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Data Master > Asset > Add',
                'category' => DB::table('category')->where('deleted', 0)->get()
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $category_id = $input['category_id'];
                $nama = $input['nama'];
                $messages = [
                    'category_id.unique' => 'Given category and name has already been taken',
                    'category_id.required' => 'Category field is required',
                    'nama.required' => 'Name field is required',
                ];
                $rules = [
                    'category_id' => [
                        'required',
                        Rule::unique('m_asset')->where(function ($query) use($category_id,$nama) {
                            return $query->where([
                                'category_id' => $category_id,
                                'nama' => $nama
                            ]);
                        })
                    ],
                    'nama' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('m_asset')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('data-master.asset.index')->withErrors($success);
            }


            return view('data-master.asset.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function edit(Request $req) {
        try {
            $data = DB::table('m_asset')->where('m_asset_id', $req->id)->first();
            $category = DB::table('category')->where('deleted', 0)->get();
            $param = [
                'title' => 'Data Master > Asset > Edit',
                'data' => $data,
                'category' => $category
            ];
            
            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $category_id = $input['category_id'];
                $nama = $input['nama'];
                $messages = [
                    'category_id.unique' => 'Given category and name has already been taken',
                    'category_id.required' => 'Category field is required',
                    'nama.required' => 'Name field is required',
                ];

                $rules = [
                    'category_id' => [
                        'required',
                        Rule::unique('m_asset')->where(function ($query) use($category_id,$nama) {
                            return $query->where([
                                'category_id' => $category_id,
                                'nama' => $nama
                            ]);
                        })->ignore($req->id, 'm_asset_id')
                    ],
                    'nama' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('m_asset')->where('m_asset_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('data-master.asset.index')->withErrors($success);
            }

            return view('data-master.asset.edit', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function delete(Request $req) {
        try {
            DB::table('m_asset')->where('m_asset_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('data-master.asset.index')->withErrors($success);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
