<?php

namespace App\Http\Controllers\DataMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class DepartmentController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $data = DB::table('department as a')
                ->join('entity as b', 'b.entity_id', '=', 'a.entity_id')
                ->select('a.*', 'b.name AS entity_name')
                ->where('a.deleted', 0)->get();

            $param = [
                'title' => 'Data Master > Department',
                'data' => $data
            ];
            return view('data-master.department.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Data Master > Department > Add',
                'entity' => DB::table('entity')->where('deleted', 0)->get()
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $entity_id = $input['entity_id'];
                $name = $input['name'];
                $messages = [
                    'entity_id.unique' => 'Given entity and name has already been taken',
                    'entity_id.required' => 'Entity field is required',
                    'name.required' => 'Name field is required',
                ];
                $rules = [
                    'entity_id' => [
                        'required',
                        Rule::unique('department')->where(function ($query) use($entity_id,$name) {
                            return $query->where([
                                'entity_id' => $entity_id,
                                'name' => $name
                            ]);
                        })
                    ],
                    'name' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('department')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('data-master.department.index')->withErrors($success);
            }


            return view('data-master.department.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function edit(Request $req) {
        try {
            $data = DB::table('department')->where('department_id', $req->id)->first();
            $entity = DB::table('entity')->where('deleted', 0)->get();
            $param = [
                'title' => 'Data Master > Department > Edit',
                'data' => $data,
                'entity' => $entity
            ];
            
            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $entity_id = $input['entity_id'];
                $name = $input['name'];
                $messages = [
                    'entity_id.unique' => 'Given entity and name has already been taken',
                    'entity_id.required' => 'Entity field is required',
                    'name.required' => 'Name field is required',
                ];

                $rules = [
                    'entity_id' => [
                        'required',
                        Rule::unique('department')->where(function ($query) use($entity_id,$name) {
                            return $query->where([
                                'entity_id' => $entity_id,
                                'name' => $name
                            ]);
                        })->ignore($req->id, 'department_id')
                    ],
                    'name' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('department')->where('department_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('data-master.department.index')->withErrors($success);
            }

            return view('data-master.department.edit', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function delete(Request $req) {
        try {
            DB::table('department')->where('department_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('data-master.department.index')->withErrors($success);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
