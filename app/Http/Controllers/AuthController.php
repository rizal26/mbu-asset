<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\Forgot;
use DB;

class AuthController extends Controller
{
    public function login(Request $req) {
        try {

            $param = [
                'title' => 'Login Page'
            ];

            if ($req->isMethod('post')) {
                $email = $req->input('email');
                $password = $req->input('password');
                $users = DB::table('v_users')->where([
                    'email' => $email,
                    'password' => $password
                ])->first();
                if (!is_null($users)) {
                    Session::put('users', $users);
                    Session::put('login', true);
                    return redirect('home');
                } else {
                    return redirect()->back()->withErrors([
                        'error' => 'Login failed'
                    ])->withInput();
                }
            }

            if (!session('login')) {
                return view('auth.login', $param);
            } else {
                return redirect('home');
            }
        } catch (\Exception $e) {
            Session::flush();
            return abort(500, $e->getMessage());
        }
    }
    
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function forgot(Request $req) {
        try {
            $param = [
                'title' => 'Forgot Page'
            ];

            if ($req->isMethod('post')) {
                $tmpPass = $this->randomPassword();
                $email =  $req->email;
                $user = DB::table('users')->where('email', $email)->first();
                if (is_null($user)) {
                    return redirect()->back()->withErrors([
                        'error' => "Sorry, we can't find email address in our system"
                    ])->withInput();
                }

                // return view('mail.forgot', ['data' => [$user, $tmpPass]]);
                Mail::to($email)->send(new Forgot([$user, $tmpPass]));
                DB::table('users')->where('user_id', $user->user_id)->update([
                    'password' => $tmpPass
                ]);
                
                return redirect()->back()->withErrors([
                    'success' => 'Temporary password has been sent to your email'
                ]);
            }

            return view('auth.forgot', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function logout(Request $req) {
        try {
            Session::flush();
            return redirect('/login');
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
