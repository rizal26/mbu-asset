<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->send();
                die();
            };
            return $next($request);
        });
    }
    
    public function index(Request $req) {
        try {

            $assetList = DB::table('asset_list')->where('deleted', 0)->get();
            $assetMt = DB::table('asset_maintenance')->where([
                'status' => 1,
                'deleted' => 0
            ])->get();

            $totalPurchase = 0;
            $totalGood = 0;
            $totalLight = 0;
            $totalHeavy = 0;
            foreach ($assetList as $key => $value) {
                $totalPurchase += $value->purchase_cost;
                if ($value->condition == 1) {
                    $totalGood += 1; 
                } elseif ($value->condition == 2) {
                    $totalLight += 1; 
                } elseif ($value->condition == 3) {
                    $totalHeavy += 1; 
                }
            }

            $totalMt = 0;
            foreach ($assetMt as $key => $value) {
                $totalMt += $value->total_cost;
            }

            $data = [
                'title' => 'Home',
                'totalAsset' => count($assetList),
                'totalPurchase' => $totalPurchase,
                'totalMt' => $totalMt,
                'totalGood' => $totalGood,
                'totalLight' => $totalLight,
                'totalHeavy' => $totalHeavy
            ];
    
            return view('home', $data);
        } catch (\Exception $e) {
            return abort(500, $e);
        }
    }

    public function chartCategory(Request $req) {
        $label = [];
        $dataset = [];
        $categories = DB::table('v_asset_list')
            ->where('deleted', 0)
            ->groupBy('category_code')
            ->select('category_code')
            ->get();
        $assetList = DB::table('v_asset_list')->where('deleted', 0)->get();
        
        foreach ($categories as $key => $value) {
            $label[] = $value->category_code; 
        }
        
        for ($i=0; $i < count($label); $i++) { 
            $good = 0;
            $light = 0;
            $heavy = 0;
            foreach ($assetList as $key => $value) {
                if ($value->category_code === $label[$i]) {
                    if ($value->condition == 1) {
                        $good += 1;
                    } elseif ($value->condition == 2) {
                        $light += 1;
                    } elseif ($value->condition == 3) {
                        $heavy += 1;
                    }
                }
            }
            $dataset[] = [$good, $light, $heavy];
        }

        return [$label,$dataset];
    }

    public function chartLocation(Request $req) {
        $label = [];
        $dataset = [];
        $categories = DB::table('v_asset_list')
            ->where('deleted', 0)
            ->groupBy('location_name')
            ->select('location_name')
            ->get();
        $assetList = DB::table('v_asset_list')->where('deleted', 0)->get();
        
        foreach ($categories as $key => $value) {
            $label[] = $value->location_name; 
        }
        
        for ($i=0; $i < count($label); $i++) { 
            $good = 0;
            $light = 0;
            $heavy = 0;
            foreach ($assetList as $key => $value) {
                if ($value->location_name === $label[$i]) {
                    if ($value->condition == 1) {
                        $good += 1;
                    } elseif ($value->condition == 2) {
                        $light += 1;
                    } elseif ($value->condition == 3) {
                        $heavy += 1;
                    }
                }
            }
            $dataset[] = [$good, $light, $heavy];
        }

        return [$label,$dataset];
    }
}
