<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DetailController extends Controller
{
    public function detail(Request $req) {
        try {
            $data = DB::table('v_asset_list')->where('asset_code', $req->code)->first();

            if (!$data) {
                return view('detail-front.notfound');
            }

            $movement = DB::table('v_asset_movement')->where('asset_id', $data->asset_id)->get();
            $maintenance = DB::table('v_asset_maintenance')->where('asset_id', $data->asset_id)->get();
            $param = [
                'title' => 'Asset Detail - '.$data->asset_code,
                'data' => $data,
                'movement' => $movement,
                'maintenance' => $maintenance,
            ];
            return view('detail-front.detail', $param);
        } catch (\Exception $e) {
            return abort(500, $e);
        }
    }
}
