<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.apikey');
    }
    
    public function get(Request $request) {
        try {
            $where = 'asset_id is not null ';
            if ($request->isMethod('post')) {
                $param = $request->all();
                foreach ($param as $key => $value) {
                    $where .= "and ".$key." = '".$param[$key]."' ";
                }
            }

            $data = DB::table('v_api')
                ->whereRaw($where)->get();
            
            return [
                'resultCode' => 0,
                'message' => 'Success',
                'data' => $data
            ];
            
        } catch (\Exception $e) {
            return response()->json([
                'resultCode' => 99,
                'message' => $e->getMessage(),
                'data' => array()
            ], 500);
        }
    }
}
