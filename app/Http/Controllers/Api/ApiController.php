<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiController extends Controller
{
    public function checkFileExist(Request $request) {
        try {
            $data = DB::table('asset_list')
                ->select('asset_id','item_code','image')
                ->where([
                    'deleted' => 0
                ])
                ->get();

            $path = public_path().'/images';
            $response = [];
            $files = [];
            
            $count = 0;
            foreach ($data as $key => $value) {
                if (!file_exists($path.'/'.$value->image)) {
                    $response['total_image'] = $count++;
                    $files[] = [
                        'id' => $value->asset_id,
                        'item_code' => $value->item_code,
                        'file_name' => $value->image
                    ];
                }
            }

            $response['file_name'] = $files;
            
            return response()->json(
                $response
            , 200);

        } catch (\Exception $e) {
            return response()->json([
                'resultCode' => 99,
                'message' => $e->getMessage(),
                'data' => array()
            ], 500);
        }
    }
}
