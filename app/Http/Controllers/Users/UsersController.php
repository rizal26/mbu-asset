<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Authentication;
use DB;

class UsersController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $where = ['deleted' => 0];
            if (!in_array(session('users')->role, [1, 7])) {
                $where['user_id'] = session('users')->user_id;
            }
            $data = DB::table('v_users')
                ->where($where)
                ->get();
            $param = [
                'title' => 'Users',
                'data' => $data
            ];
            return view('users.index', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function add(Request $req) {
        try {
            if (!in_array(session('users')->role, [1,7])) {
                return redirect()->back()->withErrors([
                    'error' => 'User not allowed to access this page'
                ])->withInput();
            }
            $location = DB::table('location')->where('deleted', 0)->get();
            $department = DB::table('department')->where('deleted', 0)->get();
            $roles = DB::table('roles')->get();
            $param = [
                'title' => 'Users > Add',
                'location' => $location,
                'department' => $department,
                'roles' => $roles
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $messages = $this->validationMsg();
                $rules = [
                    'npk' => 'required|string|unique:users,npk',
                    'email' => 'required|email|unique:users,email',
                    'name' => 'required|string|min:3|max:100',
                    'phone' => 'required|string|min:10|max:13',
                    'location_id' => 'required',
                    'department_id' => 'required',
                    'password' => 'required|confirmed|min:8',
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                unset($input['password_confirmation']);
                // return view('mail.auth', $input);
                Mail::to($input['email'])->send(new Authentication($input));
                DB::table('users')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('users.index')->withErrors($success);
            }

            return view('users.add', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function edit(Request $req) {
        try {
            if (!in_array(session('users')->role, [1,7])) {
                if ($req->id != session('users')->user_id) {
                    return redirect()->back()->withErrors([
                        'error' => 'User not allowed to access this page'
                    ])->withInput();
                }
            }
            $data = DB::table('v_users')->where('user_id', $req->id)->first();
            $location = DB::table('location')->where('deleted', 0)->get();
            $department = DB::table('department')->where('deleted', 0)->get();
            $roles = DB::table('roles')->get();
            $param = [
                'title' => 'Users > Edit',
                'data' => $data,
                'location' => $location,
                'department' => $department,
                'roles' => $roles
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $messages = $this->validationMsg();
                $rules = [
                    'npk' => 'required|string|unique:users,npk,' . $req->npk . ',npk',
                    'email' => 'required|email|unique:users,email,' . $req->email . ',email',
                    'name' => 'required|string|min:3|max:100',
                    'phone' => 'required|string|min:10|max:13'
                ];
                
                if (in_array(session('users')->role, [1,7])) {
                    $rules['location_id'] = 'required';
                    $rules['department_id'] = 'required';
                }

                if (!is_null($input['password'])) {
                    $rules['password'] = 'required|confirmed|min:8';
                } else {
                    unset($input['password']);
                }
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                unset($input['password_confirmation']);
                DB::table('users')->where('user_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('users.index')->withErrors($success);
            }

            return view('users.edit', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function delete(Request $req) {
        try {
            if (!in_array(session('users')->role, [1,7])) {
                if ($req->id != session('users')->user_id) {
                    return redirect()->back()->withErrors([
                        'error' => 'User not allowed to access this page'
                    ])->withInput();
                }
            }
            DB::table('users')->where('user_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('users.index')->withErrors($success);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    function validationMsg() {
        return [
            'npk.unique' => 'Npk has already been taken',
            'npk.required' => 'Npk field is required',
            'email.unique' => 'Email has already been taken',
            'email.required' => 'Email field is required',
            'name.required' => 'Name field is required',
            'phone.required' => 'Phone field is required',
            'phone.min' => 'Phone must be more than 9 and less than 13 numbers',
            'phone.max' => 'Phone must be more than 9 and less than 13 numbers',
            'location.required' => 'Location field is required',
            'department.required' => 'Department field is required',
            'password.required' => 'Password field is required',
            'password.confirmed' => 'Confirm Password must be equal',
        ];
    }

    public function ajaxGet(Request $req) {
        try {
            $data = DB::table('users as a')
                ->leftJoin('department as b', 'a.department_id', '=', 'b.department_id')
                ->leftJoin('location as c', 'a.location_id', '=', 'c.location_id')
                ->where('a.name', $req->name)
                ->select('a.*', 'b.name AS department_name', 'c.name AS location_name')
                ->first();

            if (is_null($data)) {
                $return = [
                    'code' => 99,
                    'data' => [],
                    'msg' => 'Data Not Found'
                ];
            } else {
                $return = [
                    'code' => 0,
                    'data' => $data,
                    'msg' => 'Success'
                ];
            }

            return $return;
        } catch (\Exception $e) {
            return [
                'code' => 99,
                'data' => [],
                'msg' => $e->getMessage()
            ];
        }
    }
}
