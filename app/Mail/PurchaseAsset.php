<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB;

class PurchaseAsset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $insert;
    public function __construct($insert)
    {
        $this->insert = $insert;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $param = $this->insert;
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject('MBU Asset Portal - Purchase Approval Request')
            ->view('mail.purchase')
            ->with($param);
    }
}
