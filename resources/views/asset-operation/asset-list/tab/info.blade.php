                                        <div class="tab-pane {{ $infoLink=='active'?'active':'fade' }}" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="{{ $infoAriaExpand }}">
                                            <!-- form -->
                                            <form class="validate-form" action="{{ route('asset-operation.asset-list.detail.edit', $data->asset_id) }}?tab=info" method="POST">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Purchase Cost</label>
                                                            <input type="text" class="form-control money" name="purchase_cost" value="{{ $data->purchase_cost }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Currency</label>
                                                            <input type="text" class="form-control" name="currency" value="{{ $data->currency }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Purchase Date</label>
                                                            <input type="text" id="fp-default" name="purchase_date" value="{{ $data->purchase_date }}" class="form-control flatpickr-basic" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">PO Number</label>
                                                            <input type="text" class="form-control" name="po_number" value="{{ $data->po_number }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Supplier</label>
                                                            <select name="supplier_id" class="form-control select2" {{ $disabled }} >
                                                                <option></option>
                                                                @foreach ($supplier as $item)
                                                                    <option value="{{ $item->supplier_id }}" 
                                                                        {{ $item->supplier_id==$data->supplier_id?'selected':'' }}>
                                                                        {{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Residual Value</label>
                                                            <input type="text" class="form-control money" name="residual_value" value="{{ $data->residual_value }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Depreciation Method</label>
                                                            @php
                                                                $arrDeprMetd = [
                                                                    1 => 'Declining Balance',
                                                                    2 => 'Double Declining Balance'
                                                                ];
                                                            @endphp
                                                            <select name="depreciation_method" class="form-control select2" {{ $disabled }}>
                                                                @foreach ($arrDeprMetd as $key => $item)
                                                                    <option value="{{ $key }}" {{ $key==$data->depreciation_method?'selected':'' }}>{{ $item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        @include('asset-operation.asset-list.tab.button')
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/ form -->
                                        </div>