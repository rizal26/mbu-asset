                                        <div class="tab-pane {{ $maintenanceLink=='active'?'active':'fade' }}" id="account-vertical-maintenance" role="tabpanel" aria-labelledby="account-pill-maintenance" aria-expanded="{{ $maintenanceAriaExpand }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="datatableMaintenance" class="table table-bordered table-striped">
                                                            <thead>
                                                                <th>ID</th>
                                                                <th>Date</th>
                                                                <th>Description</th>
                                                                <th>Total Cost</th>
                                                                <th>Vendor</th>
                                                                <th>Status</th>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                    $sum = 0;
                                                                @endphp
                                                                @foreach ($maintenance as $item)
                                                                    @php
                                                                        $sum += $item->actual_cost;
                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $item->request_code }}</td>
                                                                        <td>{{ date('d-M-Y', strtotime($item->created_at)) }}</td>
                                                                        <td>{{ $item->remarks }}</td>
                                                                        <td>{{ $item->currency.' '.number_format($item->actual_cost) }}</td>
                                                                        <td>{{ $item->supplier_name }}</td>
                                                                        <td>
                                                                            <div class="badge badge-pill badge-glow badge-{{ $item->status==0?$item->order_status==10?'success':'primary':'danger' }}"> {{ $item->status==0?$item->order_status_name:'Rejected' }} </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <p>Total maintenance cost: IDR {{ number_format($sum) }}</p>
                                                </div>
                                            </div>
                                        </div>