<script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/mask/jquery.mask.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $('select[name=m_asset_id]').trigger("change");
        $('.money').mask('000,000,000,000,000,000', {reverse: true});
        
        var basicPickr = $('.flatpickr-basic');
        if (basicPickr.length) {
            basicPickr.flatpickr();
        }

        var table = $('table').DataTable({
            scrollX: true,
            drawCallback: function( settings ) {
                feather.replace();
            },
            order: [[0, 'desc']],
        });
        
        setInterval(() => {
            table.draw()
        }, 200);
    });
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.select2').select2({
        placeholder: 'Select',
        allowClear: true
    });

    // js general tab
    $('select[name=m_asset_id]').change(function (e) { 
        e.preventDefault();
        if ($(this).val() == '') {
            $('#categoryName').val('');
        } else {
            $.ajax({
                type: 'post',
                url: "{{ route('select.asset') }}",
                data: { m_asset_id: $(this).val() },
                success: function (response) {
                    if (response.id != 1) {
                        $('.transport-form').addClass('hidden');
                    } else {
                        $('.transport-form').removeClass('hidden');
                    }
                    $('#categoryName').val(response.data);
                }
            });
        }
    });


    //js location tab
    $('#location_id').change(function (e) { 
        e.preventDefault();
        $('#building_id').html('');
        $('#area_id').html('');
        $('#floor').val('');
    });

    $('#building_id').change(function (e) { 
        e.preventDefault();
        $('#area_id').html('');
        $('#floor').val('');
    });

    $('#entity_id').change(function (e) { 
        e.preventDefault();
        $('#department_id').html('');
    });

    $('#building_id').select2({
        placeholder: 'Select',
        allowClear: true,
        ajax: {
            url: "{{ route('select.building') }}",
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                var qry = {
                    search: params.term,
                    location_id: $('#location_id').val()
                }
                return qry;
            }
        }
    });

    $('#area_id').select2({
        placeholder: 'Select',
        allowClear: true,
        ajax: {
            url: "{{ route('select.area') }}",
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                var qry = {
                    search: params.term,
                    building_id: $('#building_id').val()
                }
                return qry;
            }
        }
    });

    $('#department_id').select2({
        placeholder: 'Select',
        allowClear: true,
        ajax: {
            url: "{{ route('select.department') }}",
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                var qry = {
                    search: params.term,
                    entity_id: $('#entity_id').val()
                }
                return qry;
            }
        }
    });

    $('#area_id').change(function (e) { 
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: "{{ route('select.floor') }}",
            data: { area_id: $(this).val() },
            beforeSend: function() {
                $('#floor').val('');
            },
            success: function (response) {
                $('#floor').val(response.data);
            }
        });
    });
</script>