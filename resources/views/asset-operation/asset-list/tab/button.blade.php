@if ($disabled == '')
<button type="submit" class="btn btn-primary mt-1 mr-1">Save changes</button>
@endif
<a href="{{ route('asset-operation.asset-list.index') }}" class="btn btn-outline-secondary mt-2">Cancel</a>