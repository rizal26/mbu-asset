@extends('templates.main')
@section('title', $title)
@section('content')

<style>
    .filter{
        width: 100%
    }
</style>

                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <button class="btn btn-success dropdown-toggle waves-effect waves-float waves-light" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Export
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2" style="">
                                                <a class="dropdown-item" id="exportlabel" href="javascript:void(0);">Label</a>
                                                <a class="dropdown-item" id="exportexcel" href="javascript:void(0);">Excel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form id="formrow">
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if (!in_array(session('users')->role, [2, 3]))
                                            <select name="location" id="location_id" class="form-control select2-location rows">
                                                @if (request()->has('location'))
                                                    @php
                                                        $locId = request()->get('location');
                                                        $locName = DB::table('location')->where('location_id', $locId)->first();
                                                    @endphp
                                                    <option value="{{ $locId }}" selected>{{ $locName->name }}</option>
                                                @endif
                                            </select>
                                            @else
                                            <input type="hidden" name="location" id="location_id" value="{{ session('users')->location_id }}">
                                            @endif
                                        </div>
                                        <div class="col-md-4">
                                            <select name="building" id="building_id" class="form-control rows">
                                                @if (request()->has('building'))
                                                @php
                                                    $buildingId = request()->get('building');
                                                    $buildingName = DB::table('building')->where('building_id', $buildingId)->first();
                                                @endphp
                                                <option value="{{ $buildingId }}" selected>{{ $buildingName->name }}</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="area" id="area_id" class="form-control rows">
                                                @if (request()->has('area'))
                                                @php
                                                    $areaId = request()->get('area');
                                                    $areaName = DB::table('area')->where('area_id', $areaId)->first();
                                                @endphp
                                                <option value="{{ $areaId }}" selected>{{ $areaName->name }}</option>
                                                @endif
                                            </select>
                                        </div>
                                        
                                    </div>
                                </br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="asset_name" id="asset_id" class="form-control rows">
                                            @if (request()->has('asset_name'))
                                                @php
                                                    $assetId = request()->get('asset_name');
                                                    $assetName = DB::table('m_asset')->where('m_asset_id', $assetId)->first();
                                                @endphp
                                                <option value="{{ $assetId }}" selected>{{ $assetName->nama }}</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="condition" id="condition_id" class="form-control rows">
                                                <option></option>
                                                @php
                                                    $arrConditions = [
                                                        '1' => 'Good',
                                                        '2' => 'Slightly Damage',
                                                        '3' => 'Totally Damage'
                                                    ]
                                                @endphp
                                                @foreach ($arrConditions as $key => $item)
                                                <option value="{{ $key }}" 
                                                @if (request()->has('condition'))
                                                    @if ($key == request()->get('condition'))
                                                        {{ 'selected' }}
                                                    @endif
                                                @endif
                                                >{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <select name="rows" class="form-control rows" style="width: fit-content">
                                                <option value="10" {{ request()->has('rows')&&request()->get('rows')==10?'selected':'' }}>10</option>
                                                <option value="50" {{ request()->has('rows')&&request()->get('rows')==50?'selected':'' }}>50</option>
                                                <option value="100" {{ request()->has('rows')&&request()->get('rows')==100?'selected':'' }}>100</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="pull-right mb-2">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->get('search','') }}" aria-describedby="button-addon2">
                                                    <div class="input-group-append" id="button-addon2">
                                                        <button type="submit" class="btn btn-outline-primary waves-effect" type="button"><i data-feather='search'></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-datatable">
                                        <div class="mb-2">
                                            <form id="formlabel" action="{{ route('asset-operation.asset-list.print-label') }}" method="post" target="_blank">
                                                {{ csrf_field() }}
                                                <input type="hidden" id="labelorexcel" name="export">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="selectall">
                                                                <label class="custom-control-label" for="selectall"></label>
                                                            </div>
                                                        </th>
                                                        <th>Image</th>
                                                        <th>Asset Name</th>
                                                        <th>Asset Code</th>
                                                        <th>Brand</th>
                                                        <th>Spesification</th>
                                                        <th>User Dept.</th>
                                                        <th>Location</th>
                                                        <th>Usage Status</th>
                                                        <th>Condition</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data as $item)
                                                        <tr>
                                                            <td class="custom-control-label" for="customCheck{{ $item->asset_id }}">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input select-id" name="asset_code[]" value="{{ $item->asset_code }}" id="customCheck{{ $item->asset_id }}">
                                                                    <label class="custom-control-label" for="customCheck{{ $item->asset_id }}"></label>
                                                                </div>
                                                            </td>
                                                            <td><button type="button" class="btn btn-icon btn-flat-primary waves-effect" data-id="{{ $item->asset_id }}" data-toggle="modal" data-target="#assetImage" title="Open Image"><i data-feather='image'></i></button></td>
                                                            <td>{{ $item->asset_name }}</td>
                                                            <td><a href="{{ route('asset-operation.asset-list.detail', $item->asset_id) }}">{{ $item->asset_code }}</a></td>
                                                            <td>{{ $item->brand }}</td>
                                                            <td>{{ $item->spec }}</td>
                                                            <td>{{ $item->dept_name }}</td>
                                                            <td>{{ $item->location_name }}</td>
                                                            <td>{{ $item->usage_status }}</td>
                                                            <td>{{ $item->condition_descr }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </form>
                                        </div>
                                    </div>
                                    <span>Total rows : {{ number_format($data->total(), 0, ',', '.') }}</span>
                                    <div class="float-right">
                                        {{ $data->links('vendor.pagination.bootstrap-4') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="assetImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel1"></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center" id="bodyImage">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="" alt="" srcset="">
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
                    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>

                    <script>
                        $(function () {
                            $('#assetImage').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                var url = '{{ url("asset-operation/asset-list/image") }}'+'/'+id;
                                console.log(url);
                                $.ajax({
                                    type: "get",
                                    url: url,
                                    beforeSend: function() {
                                        var spinner = '<div class="spinner-border" style="width: 3rem; height: 3rem" role="status">'+
                                                            '<span class="sr-only">Loading...</span>'+
                                                        '</div>';
                                        $('#bodyImage').html(spinner)
                                        $('#myModalLabel1').html('');
                                    },
                                    success: function (response) {
                                        var html = '';
                                        var title = '';
                                        if (response.code == 0) {
                                            html = '<img width="400" src="'+response.src+'">';          
                                        } else {
                                            html = response.msg;          
                                        }

                                        $('#myModalLabel1').html(response.title);
                                        $('#bodyImage').html(html);
                                    }
                                });
                            });

                            $('#datatable').DataTable({
                                bFilter: false,
                                bInfo: false,
                                bPaginate: false,
                                scrollX: true,
                                ordering: false
                            });

                            $('.rows').change(function (e) { 
                                e.preventDefault();
                                let val = $(this).val();
                                $('#formrow').submit();
                            });

                            $('#selectall').change(function (e) { 
                                e.preventDefault();
                                if ($(this).is(':checked')) {
                                    $('.select-id').prop('checked',true);
                                } else {
                                    $('.select-id').prop('checked',false);
                                }
                            });

                            $('#exportlabel').click(function (e) { 
                                e.preventDefault();
                                $('#labelorexcel').val('label');
                                $('#formlabel').submit();
                            });

                            $('#exportexcel').click(function (e) { 
                                e.preventDefault();
                                $('#labelorexcel').val('excel');
                                $('#formlabel').submit();
                            });

                            $('#location_id').change(function (e) { 
                                e.preventDefault();
                                $('#building_id').val('').trigger('change');
                                $('#area_id').val('').trigger('change');
                            });

                            $('#building_id').change(function (e) { 
                                e.preventDefault();
                                $('#area_id').val('').trigger('change');
                            });

                            $('.select2-location').select2({
                                placeholder: 'Location',
                                allowClear: true,
                                ajax: {
                                    url: "{{ route('select.location') }}",
                                    type: 'POST',
                                    dataType: 'json',
                                    data: function (params) {
                                        var qry = {
                                            search: params.term,
                                        }
                                        return qry;
                                    }
                                }
                            });

                            $('#building_id').select2(
                                {
                                    placeholder: 'Building Name',
                                    allowClear: true,
                                    ajax: {
                                        url: "{{ route('data-master.building.select') }}",
                                        type: 'POST',
                                        dataType: 'json',
                                        data: function (params) {
                                            var qry = {
                                                search: params.term,
                                                location_id: $('#location_id').val(),
                                            }
                                            return qry;
                                        }
                                    }
                                }
                            );

                            $('#area_id').select2(
                                {
                                    placeholder: 'Area Name',
                                    allowClear: true,
                                    ajax: {
                                        url: "{{ route('data-master.area.select') }}",
                                        type: 'POST',
                                        dataType: 'json',
                                        data: function (params) {
                                            var qry = {
                                                search: params.term,
                                                building_id: $('#building_id').val(),
                                            }
                                            return qry;
                                        }
                                    }
                                }
                            );
                            $('#condition_id').select2(
                                {
                                    placeholder: 'Condition',
                                    allowClear: true,
                                }
                            );

                            $('#asset_id').select2({
                                placeholder: 'Asset Name',
                                allowClear: true,
                                 ajax: {
                                    url: "{{ route('select.m-asset') }}",
                                    type: 'POST',
                                    dataType: 'json',
                                    data: function (params) {
                                        var qry = {
                                            search: params.term,
                                        }
                                        return qry;
                                    }
                                }
                            });
                        });
                    </script>


@endsection