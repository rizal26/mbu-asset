@extends('templates.main')
@section('title', $title)
@section('content')
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $title }}</h4><br>
                                    <div class="float-right">
                                        @if (in_array(session('users')->role, [3]))
                                            @if ($data->status == 0 && session('users')->role != 2)
                                                <button class="btn btn-success waves-effect waves-float waves-light" data-id="{{ $data->maintenance_id }}" data-toggle="modal" data-target="#approve">Approve</button>
                                                <button class="btn btn-danger waves-effect waves-float waves-light" data-id="{{ $data->maintenance_id }}" data-toggle="modal" data-target="#reject">Reject</button>
                                            @endif
                                        @endif
                                        <a href="{{ route('asset-operation.asset-maintenance.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped">
                                                <tr>
                                                    <td><b>Created By</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->creator_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Created Date</b></td>
                                                    <td>:</td>
                                                    <td>{{ date('d-M-Y', strtotime($data->created_date)) }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Current Status<br>(Officer Head)</b></td>
                                                    <td>:</td>
                                                    <td>
                                                        @php
                                                            $status = 'Undifined';
                                                            if ($data->status == 0) {
                                                                $status = '<div class="badge badge-pill badge-glow badge-warning">Waiting for approval</div>';
                                                            } elseif ($data->status == 1) {
                                                                $status = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                                                            } else {
                                                                $status = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                                                            }
                                                        @endphp
                                                        <?=$status?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Approval By</b></td>
                                                    <td>:</td>
                                                    <td>{{ !is_null($data->approver_name)?$data->approver_name:'-' }}</td>
                                                </tr>
                                                @if ($data->status == 2)
                                                    <tr>
                                                        <td><b>Reason Rejected</td>
                                                        <td>:</td>
                                                        <td>{{ $data->reason_rejected }}</td>
                                                    </tr>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    {{-- <form class="form form-horizontal" method="post" action="{{ route('asset-operation.asset-maintenance.add') }}">
                                        {{ csrf_field() }} --}}
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="asset_id">Asset Code</label>
                                                <input type="text" class="form-control" value="{{ $data->asset_code }}" disabled>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="currency">Currency (IDR/USD/EUR)</label>
                                                <input type="text" name="currency" id="currency" class="form-control" placeholder="Currency" value="{{ $data->currency }}" disabled />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="asset_name">Asset Name</label>
                                                <input type="text" id="asset_name" class="form-control" placeholder="Asset Name" value="{{ $data->asset_name }}" readonly disabled />
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="total_cost">Total Cost</label>
                                                <input type="text" name="total_cost" id="total_cost" class="form-control money" placeholder="Total Cost" value="{{ $data->total_cost }}" disabled/>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="dept_name">Department</label>
                                                <input type="text" id="dept_name" class="form-control" placeholder="Department" value="{{ $data->dept_name }}" readonly disabled/>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="supplier_id">Vendor</label>
                                                <input type="text" class="form-control" value="{{ $data->vendor_name }}" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="descr">Description</label>
                                                <textarea name="descr" id="descr" rows="5" class="form-control" placeholder="Description" disabled>{{ $data->descr }}</textarea>
                                            </div>
                                        </div>
                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-operation.asset-maintenance.approve','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Approve Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want approve this maintenance asset ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-operation.asset-maintenance.reject','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Reject Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want reject this maintenance asset ?</p>
                                        <label for="">Reason message :</label>
                                        <textarea name="reason" cols="30" placeholder="Reason of rejection" class="form-control" required></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <script>
                        $(function () {
                            $('#approve').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });

                            $('#reject').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });
                        });
                    </script>
                                
@endsection