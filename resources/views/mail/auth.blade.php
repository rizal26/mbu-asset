<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        .bootstrap-btn {
            display: inline-block;
            font-weight: 400;
            color: #212529;
            text-align: center;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            color: #fff;
            background-color: #007bff;
            text-decoration: none;
        }


        .bootstrap-btn:hover{
            background-color: #0069d9;

        }
    </style>
    <title>Authentication</title>
  </head>
  <body>
    <div style="margin: 30px; font-family: Arial, Helvetica, sans-serif;">
        <p><b>Hallo {{ $name }},</b></p>
        <p>Congratulations, your account has been activated.<br>
        Now you can access Mitra Berlian Unggas Asset Portal System.</p>
    
        <p>This following is your login information :</p>
        <table>
            <tbody>
                <tr>
                    <td><b>Username/Email</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $email }}</td>
                </tr>
                <tr>
                    <td><b>Password</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $password }}</td>
                </tr>
            </tbody>
        </table>
        <p>Or you can directly login by click the button below</p>
        <a href="{{ route('login').'?email='.$email.'&pass='.base64_encode($password) }}" class="bootstrap-btn" target="_blank">Login</a>
        <br>
        <br>
        <p><b>Best Regards,<br>
        Admin Mitra Berlian Unggas</b><br>
        Jl. Mayang Padmi Kulon No. 31A<br>
        Kota Baru Parahyangan<br>
        Bandung, Jawa Barat<br>
        Indonesia 40553<br>
        +62 22 1000 189
        </p>
    </div>
  </body>
</html>

