<p><b>Current Asset Condition</b></p>
<table style="width: 40%">
    <tr>
        <td>Good</td>
        <td style="text-align: right" id="good"></td>
    </tr>
    <tr>
        <td>Light Damage</td>
        <td style="text-align: right" id="damage"></td>
    </tr>
    <tr>
        <td>Totally Damage</td>
        <td style="text-align: right" id="highDamage"></td>
    </tr>
    <tr>
        <td><b>Total Asset</b></td>
        <td style="text-align: right"><b id="totalAsset"></b></td>
    </tr>
</table>