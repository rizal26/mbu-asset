@extends('templates.main')
@section('title', $title)
@section('content')
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $title }}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            {{-- <div class="btn-group">
                                                <button class="btn btn-success dropdown-toggle waves-effect waves-float waves-light" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Export
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2" style="">
                                                    <a class="dropdown-item" id="exportexcel" href="javascript:void(0);">Excel</a>
                                                </div>
                                            </div> --}}
                                        <a href="{{ route('asset-purchase.step1') }}" class="btn btn-outline-primary waves-effect">Add New</a>
                                        </div>
                                        <div class="col-md-1">
                                            <form id="formrow">
                                                <select name="rows" id="rows" class="form-control" style="width: fit-content">
                                                    <option value="10" {{ request()->has('rows')&&request()->get('rows')==10?'selected':'' }}>10</option>
                                                    <option value="50" {{ request()->has('rows')&&request()->get('rows')==50?'selected':'' }}>50</option>
                                                    <option value="100" {{ request()->has('rows')&&request()->get('rows')==100?'selected':'' }}>100</option>
                                                </select>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="pull-right mb-2">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->get('search','') }}" aria-describedby="button-addon2">
                                                    <div class="input-group-append" id="button-addon2">
                                                        <button type="submit" class="btn btn-outline-primary waves-effect" type="button"><i data-feather='search'></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-datatable">
                                        <div class="mb-2">
                                            <form id="formlabel" action="{{ route('asset-purchase.print') }}" method="post" target="_blank">
                                                {{ csrf_field() }}
                                                <input type="hidden" id="labelorexcel" name="export">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Request ID</th>
                                                        <th>Asset Name</th>
                                                        <th>category</th>
                                                        <th>Department</th>
                                                        <th>Priority</th>
                                                        <th>Order Status</th>
                                                        <th>Expected Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data as $item)
                                                        <tr>
                                                            <td>{{ $item->request_code }}</td>
                                                            <td>{{ $item->asset_name }}</td>
                                                            <td>{{ $item->category_name }}</td>
                                                            <td>{{ $item->dept_name }}</td>
                                                            <td>
                                                                @if ($item->priority == 1)
                                                                    <div class="badge badge-pill badge-glow badge-danger">{{ $item->priority_name }}</div>
                                                                @elseif ($item->priority == 2)
                                                                    <div class="badge badge-pill badge-glow badge-warning">{{ $item->priority_name }}</div>
                                                                @else 
                                                                    <div class="badge badge-pill badge-glow badge-success">{{ $item->priority_name }}</div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="badge badge-pill badge-glow badge-{{ $item->status==0?$item->order_status==10?'success':'primary':'danger' }}"> {{ $item->status==0?$item->order_status_name:'Rejected' }} </div>
                                                            </td>
                                                            <td>{{ $item->expected_date_formated }}</td>
                                                            <td>
                                                                <a href="{{ route("asset-purchase.detail", $item->request_id) }}" class="btn btn-icon btn-flat-primary waves-effect" title="View Detail"><i data-feather="alert-circle"></i></a>
                                                                @if (session('users')->role == 7)
                                                                    @if ($item->order_status == 10 && $item->validate == 0)
                                                                        <button type="button" class="btn btn-sm btn-danger" data-id="{{ $item->request_id }}" data-toggle="modal" data-target="#validate">Need Validation</button>
                                                                    @elseif ($item->order_status == 10 && $item->validate == 1)
                                                                        <button type="button" class="btn btn-sm btn-success">Validated</button>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </form>
                                        </div>
                                    </div>
                                    <span>Total rows : {{ number_format($data->total(), 0, ',', '.') }}</span>
                                    <div class="float-right">
                                        {{ $data->links('vendor.pagination.bootstrap-4') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="validate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-purchase.validate','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Validate Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <input type="hidden" name="status" id="act" value="1">
                                        <p>Are you sure want to validate this request ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>

                    <script>
                        $(function () {
                            $('#datatable').DataTable({
                                bFilter: false,
                                bInfo: false,
                                bPaginate: false,
                                scrollX: true,
                                ordering: false
                            });

                            $('#rows').change(function (e) { 
                                e.preventDefault();
                                let val = $(this).val();
                                $('#formrow').submit();
                            });

                            $('#validate').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });
                        });
                    </script>


@endsection