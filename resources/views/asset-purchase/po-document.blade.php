<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <style>
        .header-tbl {
            background-color: #2F75B5;
            color: white;
        }

        body {
            color: black
        }

        .table-padleft td {
            padding-left: 20px;
        }

        .tbl-border td {
            border: 1px solid rgb(164, 164, 164);
        }

        .tbl-border {
            border-collapse: collapse;
        }
        
    </style>
    <title>Purchase Order</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{public_path('app-assets/images/logo/favicon_new.png')}}">
</head>
<body>
    <div class="container">
        <div style="float:right !important;">
            <span style="font-size: 40px;"><b>PURCHASE ORDER</b></span>
        </div>

        <img src="{{ public_path('assets/images/logo_mbu_primary.png') }}" style="width: 28%; margin-top: 4rem">
        <br><br>
        <table style="width: 100%">
            <td style="width: 50%">
                <span><b>PT MITRA BERLIAN UNGGAS</b></span><br>
                <span>
                    Gedung Soho PVJ Lt. 3, Jl. Karang Tinggal, Cipedes<br>
                    Kec. Sukajadi, Kota Bandung,<br>
                    Jawa Barat 40162
                </span><br>
                <div style="font-size: 13px; color: #2F75B5; text-decoration: underline;">
                    info@mbugroup.id
                    <span style="margin-left: 8.5rem;">www.mbugroup.id</span>
                </div>
            </td>
            <td style="vertical-align: baseline;">
                <table style="width: 100%">
                    <td>
                        <table style="text-align: center; width: 100%">
                            <tr>
                                <td class="header-tbl">PO NUMBER</td>
                            </tr>
                            <tr>
                                <td>{{ 'PO-'.str_pad($data->request_id, 5, '0', STR_PAD_LEFT), }}</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 60px;"></td>
                    <td>
                        <table style="text-align: center; width: 100%">
                            <tr>
                                <td class="header-tbl">DATE</td>
                            </tr>
                            <tr>
                                <td>{{ date('F d, Y') }}</td>
                            </tr>
                        </table>
                    </td>
                </table>
                {{-- <div style="display: flex">
                    <div style="width: 45%">
                        
                    </div>
                    <div style="width: 45%; margin-left: auto; order: 2;">
                        
                    </div>
                </div> --}}
            </td>
        </table>
        <br>
        <table style="width: 100%">
            <td style="width: 50%">
                <table style="width: 100%" class="table-padleft">
                    <tr>
                        <td class="header-tbl">VENDOR</td>
                    </tr>
                    <tr>
                        <td>
                            {{ $data->supplier_name }}<br>
                            {{ $data->supplier_pic }}<br>
                            {{ $data->supplier_phone.' / '.$data->supplier_email }}<br>
                            {{ $data->supplier_address }}
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: baseline;">
                <table style="width: 100%" class="table-padleft">
                    <tr>
                        <td class="header-tbl">SHIP TO</td>
                    </tr>
                    <tr>
                        <td>
                            PT Mitra Berlian Unggas<br>
                            {{ $data->creator_name }}<br>
                            {{ $data->creator_phone.' / '.$data->creator_email }}<br>
                            {{ $data->shipping_address }}
                        </td>
                    </tr>
                </table>
            </td>
        </table>
        <br>
        <table style="width: 100%" class="tbl-border">
            <tr>
                <td class="header-tbl" style="width: 25%; text-align: center">
                    REQUESTIONER
                </td>
                <td class="header-tbl" style="width: 25%; text-align: center">
                    SHIP VIA
                </td>
                <td class="header-tbl" style="width: 25%; text-align: center">
                    F.O.B
                </td>
                <td class="header-tbl" style="width: 25%; text-align: center">
                    SHIPPING TERMS
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
        </table>
        <br>
        <table style="width: 100%; text-align: center;" class="tbl-border">
            <tr style="height: 50px;">
                <td class="header-tbl">ITEM DESCRIPTION</td>
                <td class="header-tbl">UNIT PRICE</td>
                <td class="header-tbl">QUANTITY</td>
                <td class="header-tbl">TOTAL AMOUNT</td>
            </tr>
            <tr style="height: 50px;">
                <td>{{ $data->asset_name }}</td>
                <td align="left" style="padding-left: 8px; padding-right: 8px;">
                    Rp
                    <span style="float:right !important;">
                        {{ number_format($data->amount/$data->qty) }}
                    </span>
                </td>
                <td>{{ $data->qty }}</td>
                <td align="left" style="padding-left: 8px; padding-right: 8px;">
                    Rp
                    <span style="float:right !important;">{{ number_format($data->amount) }}</span>
                </td>
            </tr>
            <tr style="font-weight: 700">
                <td colspan="3">GRAND TOTAL</td>
                <td align="left" style="padding-left: 8px; padding-right: 8px;">
                    Rp
                    <span style="float:right !important;">{{ number_format($data->amount) }}</span>
                </td>
            </tr>
        </table>
        <br>
        <span style="float:right !important; vertical-align: baseline;">PT MITRA BERLIAN UNGGAS</span>
        <table style="width: 50%;">
            <tr>
                <td style="text-align: center; background-color: #D8E1F2">Special Instructions</td>
            </tr>
            <tr>
                <td style="text-align: left; padding-left: 20px;">{{ $data->special_instruction }}</td>
            </tr>
        </table>
    </div>    
</body>
</html>