@extends('templates.main')
@section('title', $title)
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.css">

            @include('asset-purchase.wizard')
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" method="post" action="{{ route('asset-purchase.step1') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="purchase_type">Purchase Type</label>
                                            <select name="purchase_type" id="purchase_type" class="form-control select2" required>
                                                <option></option>
                                                @php
                                                    $arrType = [0 => 'New Asset',1 => 'Maintenance'];
                                                @endphp
                                                @foreach ($arrType as $key => $item)
                                                <option value="{{ $key }}" {{ old('purchase_type')==$key?'selected':'' }}>{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6" style="display: none" id="select_asset_id">
                                            <label class="form-label" for="asset_id">Asset Code</label>
                                                <select class="select2 w-100" name="asset_id" id="asset_id" >
                                                    <option></option>
                                                    @if (old('asset_id'))
                                                        @php
                                                            $asset = DB::table('asset_list')->where('asset_id', old('asset_id'))->first();
                                                        @endphp
                                                        <option value="{{ $asset->asset_id }}" selected>{{ $asset->item_code }}</option>
                                                    @endif
                                                </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="brand">Brand</label>
                                            <input type="text" class="form-control" name="brand" id="brand" placeholder="Brand" value="{{ old('brand') }}" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="m_asset_id">Item Name</label>
                                            <select name="m_asset_id" id="m_asset_id" class="form-control select2" required>
                                                <option></option>
                                                @foreach ($m_asset as $item)
                                                <option value="{{ $item->m_asset_id }}" {{ old('m_asset_id')==$item->m_asset_id?'selected':'' }}>{{ $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="category">Category</label>
                                            <input type="text" id="category" class="form-control" placeholder="Category" readonly required />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="spec">Specification</label>
                                            <input type="text" name="spec" id="spec" class="form-control" placeholder="Specification" value="{{ old('spec') }}" required/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="qty">Quantity</label>
                                            <input type="number" name="qty" class="form-control" id="qty" placeholder="Quantity" value="{{ old('qty') }}" required/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="uom">UOM</label>
                                            <select class="select2 form-control w-100" name="uom" id="uom" required>
                                                <option></option>
                                                @foreach ($uom as $item)
                                                    <option value="{{ $item->uom_id }}" {{ old('uom_id')==$item->uom_id?'selected':'' }}>{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="expected_date">Expected Date</label>
                                            <input type="text" class="form-control flatpickr-basic" placeholder="Asset Delivered" name="expected_date" value="{{ old('expected_date') }}" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="priority">Priority Level</label>
                                            <select name="priority" class="form-control select2" required>
                                                <option></option>
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="remarks">Remarks</label>
                                            <textarea name="remarks" cols="30" class="form-control" placeholder="Remarks" required></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            @include('asset-purchase.asset-condition')
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                            <a href="{{ route('asset-purchase.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                <script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
                <script>
                    $(function () {
                        $('select[name=m_asset_id]').trigger("change");
                        var basicPickr = $('.flatpickr-basic');
                        if (basicPickr.length) {
                            basicPickr.flatpickr();
                        }
                        $('.select2').select2({
                            placeholder: 'Select',
                            allowClear: true
                        });

                         $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $('#purchase_type').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 1) { // maintenance
                                $('#select_asset_id').show();
                                $('#asset_id').attr('required', true);
                                $('#brand').attr('readonly', true);
                                $('#m_asset_id').prop('disabled', true);
                                $('#spec').attr('readonly', true);
                                $('#qty').attr('readonly', true);
                                $('#qty').removeAttr('required');
                                $('#uom').prop('disabled', true);
                                
                            } else { // new asset
                                $('#select_asset_id').hide();
                                $('#asset_id').removeAttr('required')
                                $('#asset_id').val('').trigger('change')

                                $('#brand').attr('readonly', false);
                                $('#m_asset_id').prop("disabled", false);
                                $('#spec').attr('readonly', false);
                                $('#qty').attr('readonly', false);
                                $('#qty').attr('required')
                                $('#uom').prop('disabled', false);
                            }
                        });

                        $('#asset_id').select2({
                            placeholder: 'Select',
                            allowClear: true,
                            ajax: {
                                url: "{{ route('select.asset-list') }}",
                                type: 'GET',
                                dataType: 'json',
                                data: function (params) {
                                    var qry = {
                                        search: params.term
                                    }
                                    return qry;
                                }
                            }
                        });

                        $('#asset_id').change(function (e) { 
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: "{{ route('asset-operation.asset-list.ajax-get') }}",
                                data: { asset_id: $(this).val() },
                                success: function (response) {
                                        $('#asset_name').val(response.data.asset_name);
                                        $('#dept_name').val(response.data.dept_name);

                                        $('#brand').val(response.data.brand);
                                        $('#m_asset_id').val(response.data.m_asset_id).trigger('change');
                                        $('#spec').val(response.data.spec);
                                        $('#qty').val(1)
                                        $('#uom').val(response.data.uom_id).trigger('change')
                                }
                            });
                        });

                        $('#m_asset_id').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == '') {
                                $('#category').val('');
                            } else {
                                $.ajax({
                                    type: 'post',
                                    url: "{{ route('select.asset') }}",
                                    data: { m_asset_id: $(this).val() },
                                    success: function (response) {
                                        $('input#category').val(response.data);
                                    }
                                });
                            }

                            var idAsset = $(this).val();
                            $.ajax({
                                type: 'GET',
                                url: "{{ url('/select/asset-condition') }}"+"/"+idAsset,
                                success: function (data) {
                                    $('#good').html(data.good);
                                    $('#damage').html(data.damage);
                                    $('#highDamage').html(data.highDamage);
                                    $('#totalAsset').html(data.totalAsset);
                                }
                            });

                        });
                    });
                </script>
@endsection