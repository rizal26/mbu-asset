        <footer class="footer footer-static footer-light">
                <div class="float-right">Version 1.0</div>
                <p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2022 Bright Creative<span class="d-none d-sm-inline-block">, All rights Reserved</span></span></p>
        </footer>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>