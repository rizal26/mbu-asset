@if($errors->any())
    @foreach($errors->all() as $error)
        @if($errors->has('success'))
        <div class="alert alert-success">
            <div class="alert-body">
                <strong>{{ $error }}</strong>
            </div>
        </div>
        @else
        <div class="alert alert-danger">
            <div class="alert-body">
                <strong>{{ $error }}</strong>
            </div>
        </div>
        @endif    
    @endforeach
@endif