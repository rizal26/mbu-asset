@extends('templates.main')
@section('title', $title)
@section('content')
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$title}}</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="post" action="{{ route('users.add') }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="code" class="float-right">NPK</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="npk" class="form-control" name="npk" placeholder="NPK" value="{{ old('npk') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Name</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email" class="float-right">Email</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="email" id="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email" class="float-right">Phone</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="number" id="phone" class="form-control" name="phone" placeholder="Phone" value="{{ old('phone') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="location" class="float-right">Location</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select name="location_id" class="form-control select2">
                                                            <option></option>
                                                            @foreach ($location as $item)
                                                                <option value="{{ $item->location_id }}" {{ old('location_id')==$item->location_id?'selected':'' }}>
                                                                    {{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email" class="float-right">Department</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select name="department_id" class="form-control select2">
                                                            <option></option>
                                                            @foreach ($department as $item)
                                                                <option value="{{ $item->department_id }}" {{ old('department_id')==$item->department_id?'selected':'' }}>
                                                                    {{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email" class="float-right">User Role</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select name="role" class="form-control select2">
                                                            <option></option>
                                                            @foreach ($roles as $key => $item)
                                                                <option value="{{ $item->id }}" {{ old('role')==$item->id?'selected':'' }}>
                                                                    {{ $item->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Password</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="password" name="password" class="form-control" placeholder="password" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Confirm Password</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                                <a href="{{ route('users.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                    <script>
                        $(function () {
                            $('select').select2({
                                placeholder: 'Select',
                                allowClear: true
                            });
                        });
                    </script>
@endsection