@extends('templates.main')
@section('title', $title)
@section('content')
            
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$title}}</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="post" action="{{ route('data-master.area.edit', $data->area_id) }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="code" class="float-right">Location</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select id="location_id" class="form-control">
                                                            <option></option>
                                                            @foreach ($location as $item)
                                                                <option value="{{ $item->location_id }}" 
                                                                    {{ $item->location_id==$data->location_id?'selected':'' }}>{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="code" class="float-right">Building</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select name="building_id" id="building_id" class="form-control">
                                                            <option value="{{ $data->building_id }}" selected>{{ $data->building_name }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Area Name</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="{{ $data->name }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Floor</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control" name="floor" placeholder="Floor" value="{{ $data->floor }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Update</button>
                                                <a href="{{ route('data-master.area.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                    <script>
                        $(function () {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            $('#location_id').select2({
                                placeholder: 'Select',
                                allowClear: true
                            });

                            $('#location_id').change(function (e) { 
                                e.preventDefault();
                                $('#building_id').html('')
                            });

                            $('#building_id').select2({
                                placeholder: 'Select',
                                allowClear: true,
                                ajax: {
                                    url: "{{ route('select.building') }}",
                                    type: 'POST',
                                    dataType: 'json',
                                    data: function (params) {
                                        var qry = {
                                            search: params.term,
                                            location_id: $('#location_id').val(),
                                        }
                                        return qry;
                                    }
                                }
                            });
                        });
                    </script>
@endsection