@extends('templates.main')
@section('title', $title)
@section('content')
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$title}}</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="post" action="{{ route('data-master.supplier.add') }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Name</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Address</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <textarea class="form-control" name="address" placeholder="Masukan alamat lengkap" required>{{ old('address') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Email</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="email" id="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Phone</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="number" id="phone" class="form-control" name="phone" placeholder="Phone" value="{{ old('phone') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">PIC</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="pic" class="form-control" name="pic" placeholder="PIC" value="{{ old('pic') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                                <a href="{{ route('data-master.supplier.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection