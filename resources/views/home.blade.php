@extends('templates.main')
@section('title', $title)
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/charts/chart-apex.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/dashboard-ecommerce.css')}}">

    <div class="row match-height">
        <div class="col-xl-2 col-md-6 col-12">
            <div class="card">
                <div class="card-body pb-50">
                    <h6>Purchase Cost</h6>
                    <h2 class="font-weight-bolder mb-1" style="font-size: {{ strlen($totalPurchase)>=10?'20px':'' }}">{{ number_format($totalPurchase) }}</h2>
                    <div id="statistics-order-chart"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-md-6 col-12">
            <div class="card card-tiny-line-stats">
                <div class="card-body pb-50">
                    <h6>Maintenance Cost</h6>
                    <h2 class="font-weight-bolder mb-1" style="font-size: {{ strlen($totalMt)>=10?'20px':'' }}">{{ number_format($totalMt) }}</h2>
                    <div id="statistics-profit-chart"></div>
                </div>
            </div>
        </div>
        <!-- Statistics Card -->
        <div class="col-xl-8 col-md-6 col-12">
            <div class="card card-statistics">
                <div class="card-header">
                    <h4 class="card-title">Asset Statistics</h4>
                </div>
                <div class="card-body statistics-body">
                    <div class="row">
                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                            <div class="media">
                                <div class="avatar bg-light-primary mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='database' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalAsset }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Asset</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                            <div class="media">
                                <div class="avatar bg-light-success mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='check-circle' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalGood }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Good Asset</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                            <div class="media">
                                <div class="avatar bg-light-warning mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='alert-circle' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalLight }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Light Damage</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6 col-12">
                            <div class="media">
                                <div class="avatar bg-light-danger mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='x-circle' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalHeavy }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Heavy Damage</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Statistics Card -->
    </div>
    <div class="row match-height">
        <div class="col-xl-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    <div class="header-left">
                        <h4 class="card-title">Asset Condition by Category</h4>
                    </div>
                </div>
                <div class="card-body">
                    <canvas class="chart-category chartjs1" data-height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    <div class="header-left">
                        <h4 class="card-title">Asset Condition by Location</h4>
                    </div>
                </div>
                <div class="card-body">
                    <canvas class="chart-location chartjs2" data-height="400"></canvas>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/charts/chart.min.js')}}"></script>
    {{-- <script src="{{asset('app-assets/js/scripts/charts/chart-chartjs.js')}}"></script> --}}
    @include('chart-category')
    @include('chart-location')
    
    <script>
        $.ajax({
            type: "GET",
            url: "{{ route('home.chart.category') }}",
            success: chartByCategory,
        });
        $.ajax({
            type: "GET",
            url: "{{ route('home.chart.location') }}",
            success: chartByLocation,
        });
    </script>


@endsection