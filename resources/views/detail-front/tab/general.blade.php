                                        <div role="tabpanel" class="tab-pane {{ $generalLink=='active'?'active':'fade' }}" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="{{ $generalAriaExpand }}">
                                            <!-- form -->
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-username">Asset Code</label>
                                                            <input type="text" class="form-control" value="{{ $data->asset_code }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Asset Name</label>
                                                            <input type="text" class="form-control" value="{{ $data->asset_name }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Category</label>
                                                            <input type="text" id="categoryName" class="form-control" value="{{ $data->category }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Brand</label>
                                                            <input type="text" class="form-control" name="brand" value="{{ $data->brand }}" placeholder="Brand" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Type / Spesification</label>
                                                            <input type="text" class="form-control" name="spec" value="{{ $data->spec }}" placeholder="Type / Spesification" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Serial Number</label>
                                                            <input type="text" class="form-control" name="sn" value="{{ $data->sn }}" placeholder="Serial Number" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    @if ($data->category_id == 1 || $data->category_code == 'TE')
                                                    <div class="col-12 col-sm-6 transport-form">
                                                        <div class="form-group">
                                                            <label for="account-name">NOPOL</label>
                                                            <input type="text" class="form-control" name="nopol" value="{{ $data->nopol }}" placeholder="Plat Nomor Polisi" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 transport-form">
                                                        <div class="form-group">
                                                            <label for="account-name">Color</label>
                                                            <input type="text" class="form-control" name="color" value="{{ $data->color }}" placeholder="Color" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 transport-form">
                                                        <div class="form-group">
                                                            <label for="account-name">CC</label>
                                                            <input type="text" class="form-control" name="cc" value="{{ $data->cc }}" placeholder="CC" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Status</label>
                                                            <input type="text" class="form-control" value="{{ $data->usage_status }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Condition</label>
                                                            <input type="text" class="form-control" value="{{ $data->condition_descr }}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
