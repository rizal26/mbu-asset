<script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/mask/jquery.mask.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>

<script>
    $(function () {
        $('.money').mask('000,000,000,000,000,000', {reverse: true});
        
        var table = $('table').DataTable({
            scrollX: true,
            resposinve: true,
            bAutoWidth: false,
            drawCallback: function( settings ) {
                feather.replace();
            },
            order: [[0, 'desc']],
        });
        
        setInterval(() => {
            table.draw()
        }, 200);
    });
</script>