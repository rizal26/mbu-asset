                                        <div class="tab-pane {{ $movementLink=='active'?'active':'fade' }}" id="account-vertical-movement" role="tabpanel" aria-labelledby="account-pill-movement" aria-expanded="{{ $movementAriaExpand }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    {{-- <div class="table-responsive"> --}}
                                                        <table id="datatableMovement" class="table table-bordered table-striped">
                                                            <thead>
                                                                <th>ID</th>
                                                                <th>Created Date</th>
                                                                <th>Previous<br>Location</th>
                                                                <th>Destination<br>Location</th>
                                                                <th>Approval</th>
                                                                <th>Validation</th>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($movement as $item)
                                                                    <tr>
                                                                        <td>{{ $item->movement_id }}</td>
                                                                        <td>{{ date('d-M-Y', strtotime($item->created_date)) }}</td>
                                                                        <td>{{ $item->prev_location }}</td>
                                                                        <td>{{ $item->dest_location }}</td>
                                                                        <td>
                                                                            @php
                                                                                $appr = 'Undifined';
                                                                                if ($item->status == 0) {
                                                                                    $appr = '<div class="badge badge-pill badge-glow badge-warning">Waiting</div>';
                                                                                } elseif ($item->status == 1) {
                                                                                    $appr = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                                                                                } else {
                                                                                    $appr = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                                                                                }
                                                                            @endphp
                                                                            <?=$appr?>
                                                                        </td>
                                                                        <td>
                                                                            @php
                                                                                 $valid = 'Undifined';
                                                                                if ($item->validate == 0) {
                                                                                    $valid = '<div class="badge badge-pill badge-glow badge-warning">Waiting</div>';
                                                                                } elseif ($item->validate == 1) {
                                                                                    $valid = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                                                                                } else {
                                                                                    $valid = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                                                                                }
                                                                            @endphp
                                                                            <?=$valid?>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    {{-- </div> --}}
                                                </div>
                                            </div>
                                        </div>