                                        <div class="tab-pane {{ $locationLink=='active'?'active':'fade' }}" id="account-vertical-location" role="tabpanel" aria-labelledby="account-pill-location" aria-expanded="{{ $locationAriaExpand }}">
                                            <!-- form -->
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-username">Location</label>
                                                            <input type="text" class="form-control" value="{{ $data->location_name }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Building</label>
                                                            <input type="text" class="form-control" value="{{ $data->building_name }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Room / Area</label>
                                                            <input type="text" class="form-control" value="{{ $data->area_name }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Floor</label>
                                                            <input type="text" name="floor" id="floor" class="form-control" value="{{ $data->floor }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">User / PIC</label>
                                                            <input type="text" class="form-control" name="assigned_user" value="{{ $data->assigned_user }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Department</label>
                                                            <input type="text" class="form-control" value="{{ $data->dept_name }}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!--/ form -->
                                        </div>